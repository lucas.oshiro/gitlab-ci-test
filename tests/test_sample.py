def magic_fun(x):
    return x + 1


#Para dar erro no teste alterar assert magic_fun(3) == 4 para assert magic_fun(3) == 5
def test_answer():
    assert magic_fun(3) == 4
