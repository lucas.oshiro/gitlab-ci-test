# Desafio Pipeline Gitlab-CI

Desafio criado para o captulo de CI/CD da Rivendel. 
Principais atividades
  - Criar uma pipeline de CI/CD com o gitlab-ci.
  - Runners precisa estar rodando em um K8s.
  - Deploy da aplicação precisa ser feita em um K8s.

### Tech
Python 3 +.
Precisa instalar as dependencias, no Python é utilizado o Pip
```sh
$ sudo pip install -r requirements.txt
```

### Executar testes
Os testes não é da aplicação em sí, foi criado um teste unitario básico só para questão de conceito.
```sh
$ cd tests/
$ pytest ou pytest -v
```

### Executar a aplicação
Aplicação sobe por default na porta 5000
```sh
$ cd clubes/
$ python main.py
$
```

### Testar a aplicação
Testar GET
```sh
$ curl http://localhost:5000/clubes
```
Testar POST
```sh
$ curl -X POST -H "Content-Type: application/json" -d '{
  "clube": "Real Madrid",
  "founded": 1902,
  "mundial": true
}' http://localhost:5000/clubes
```

Dúvidas falar com rafael.oliveira@rivendel.com.br
----

