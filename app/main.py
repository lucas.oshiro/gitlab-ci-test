#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Flask, jsonify, request
import pytest


app = Flask(__name__)

time = [
  { 'clube': 'Sao Paulos FC', 'founded': 1930, 'mundial': True },
  { 'clube': 'Palmeiras FC', 'founded': 1914 , 'mundial': False},
]


@app.route('/clubes')
def get_clubes():
  return jsonify(time), 200


@app.route('/clubes', methods=['POST'])
def add_clubes():
  print(request.get_json())
  time.append(request.get_json())
  return '', 204


@app.route('/status')
def status():
  status = {'status' : 'up'}
  return jsonify(status), 200

#Teste unitario  alterar o valor de assert func(3) == 4 para assert func(3) == 5
#Assim vai dar erro no teste


if __name__ == '__main__':
  app.run(debug=True, host='0.0.0.0')
